---
layout: page
title: Om os
permalink: /om/
---

**data.coop** er en forening og et kooperativ. Visionen
er, at vi – medlemmerne i kooperativet – ejer vores egne data.

Dette indebærer en del og har som konsekvens, at vi bliver nødt til at eje
vores egen infrastruktur og have indblik i og kontrol over den software,
som vi bruger til at kommunikere på nettet. Ret forsimplet betyder dette:

 * At vi ejer vores egen hardware
 * At vi kun bruger open source software

Det hele bliver meget hurtigt meget teknisk og der er også en klar overvægt af
teknisk interesserede blandt vores medlemmer. Men alle er velkomne, så længe
de kan stå inde for [vores værdier](https://git.data.coop/data.coop/dokumenter/src/branch/master/Vedtaegter.md)
og overholder vores [Acceptable Usage Policy](https://git.data.coop/data.coop/dokumenter/src/branch/master/Acceptable%20Usage%20Policy.md).

Vi driver en række [tjenester](/tjenester/), baseret på open source software,
til fx kalender, dokumentdeling og sociale medier. Mere er på vej. Men da alt
arbejde foregår på frivilligt basis, tager ting tid at få på plads.

Drømmene er store, men vi har også begge ben på jorden. Så for rent faktisk at
kunne komme i gang, kunne vi ikke stille urealistiske krav: At køre åben
hardware eller drive tjenester som fx egen søgemaskine er således ikke inden
for rammerne.

Foreningen råder over 2 rack-servere. Vi er ved at opbygge et medlemssystem
og afprøver nogle prototyper til den fremtidige hosting og infrastruktur.

Du kan finde os på:

 * Matrix: **[#data.coop:data.coop](https://matrix.to/#/#data.coop:data.coop)**
 * IRC (Libera.chat): **#data.coop**
 * Vores [Gitea server](https://git.data.coop/data.coop/)
