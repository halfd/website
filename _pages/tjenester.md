---
layout: page
title: Tjenester
permalink: /tjenester/
---

Her er en oversigt over de tjenester vi tilbyder. Nogle er [kun for vores
medlemmer](#tjenester-der-kun-er-for-vores-medlemmer), andre er [åbne for
alle](#tjenester-der-er-åbne-for-alle).

Alle tjenester er markeret med nogle badges, der deklarerer i hvor høj grad den
enkelte tjeneste lever op til [kerneprincipperne defineret i formålsparagraffen
i vores vedtægter](/_pages/vedtaegter.html#-2-form%C3%A5l). Vi har også skrevet
en uddybning af [hvad de forskellige badges dækker over](/tjenester/badges/).

## Tjenester der kun er for vores medlemmer

Er du medlem og vil have adgang til en tjeneste? Så kontakt en fra bestyrelsen.
Enten via <a href="/om/">en af vores chatkanaler</a> eller <a href="mailto:board@data.coop">e-mail</a>.

<h3 class="service mastodon"><a href="https://social.data.coop/">Mastodon</a> – micro-blogging, socialt network</h3>

<section class="service-details">
    <p>Del kattebilleder og memes og følg med i hvad andre spændende mennesker foretager sig.</p>
    <h4>Badges</h4>
    <ul class="badges">
        <li>
            <span class="badge badge-positive badge-stable">Positiv:</span>
            <a href="/tjenester/badges/#stabilitet">
                Tjenesten er stabil
            </a>
        </li>
        <li>
            <span class="badge badge-positive badge-secure-connection">Positiv:</span>
            <a href="/tjenester/badges/#sikker-forbindelse">
                Sikker forbindelse
            </a>
        </li>
        <li>
            <span class="badge badge-negative badge-encrypted-data-storage">Negativ:</span>
            <a href="/tjenester/badges/#krypteret-opbevaring-af-data">
                Data opbevares <em>ikke</em> krypteret
            </a>
        </li>
        <li>
            <span class="badge badge-negative badge-zero-knowledge">Negativ:</span>
            <a href="/tjenester/badges/#zero-knowledge">
                <em>Ikke</em> omfattet af zero knowledge
            </a>
        </li>
        <li>
            <span class="badge badge-positive badge-backup">Positiv:</span>
            <a href="/tjenester/badges/#backup">
                Der er backup af data
            </a>
        </li>
        <li>
            <span class="badge badge-negative badge-logging">Negativ:</span>
            <a href="/tjenester/badges/#logning">
                Der foretages logning
            </a>
        </li>
        <li>
            <span class="badge badge-partial badge-anonymous-access">Delvis:</span>
            <a href="/tjenester/badges/#anonym-adgang">
                Delvis anonym adgang
            </a>[1]
        </li>
    </ul>

    <p class="badge-notes">
        [1] Offentlig aktivitet på tjenesten kan tilgås af alle, uden at være logget ind.
        Det kræver en konto at dele sine egne ting på Mastodon.
    </p>
</section>

<h3 class="service nextcloud"><a href="https://cloud.data.coop/">Nextcloud</a> – filer, kalender, adressebog</h3>

<section class="service-details">
    <p>Gem dine filer, din kalender og dine kontakter i skyen og tilgå alt fra alle dine enheder.</p>
    <h4>Badges</h4>
    <ul class="badges">
        <li>
            <span class="badge badge-positive badge-stable">Positiv:</span>
            <a href="/tjenester/badges/#stabilitet">
                Tjenesten er stabil
            </a>
        </li>
        <li>
            <span class="badge badge-positive badge-secure-connection">Positiv:</span>
            <a href="/tjenester/badges/#sikker-forbindelse">
                Sikker forbindelse
            </a>
        </li>
        <li>
            <span class="badge badge-negative badge-encrypted-data-storage">Negativ:</span>
            <a href="/tjenester/badges/#krypteret-opbevaring-af-data">
                Data opbevares <em>ikke</em> krypteret
            </a>
        </li>
        <li>
            <span class="badge badge-negative badge-zero-knowledge">Negativ:</span>
            <a href="/tjenester/badges/#zero-knowledge">
                <em>Ikke</em> omfattet af zero knowledge
            </a>
        </li>
        <li>
            <span class="badge badge-positive badge-backup">Positiv:</span>
            <a href="/tjenester/badges/#backup">
                Der er backup af data
            </a>
        </li>
        <li>
            <span class="badge badge-negative badge-logging">Negativ:</span>
            <a href="/tjenester/badges/#logning">
                Der foretages logning
            </a>
        </li>
        <li>
            <span class="badge badge-partial badge-anonymous-access">Delvis:</span>
            <a href="/tjenester/badges/#anonym-adgang">
                Delvis anonym adgang
            </a>[1]
        </li>
    </ul>

    <p class="badge-notes">
        [1] Det kræver login at sende data til skyen. Men det er muligt at dele
        dine data med andre (eller dig selv) uden at de skal logge ind.
    </p>
</section>


## Tjenester der er åbne for alle

<h3 class="service rallly"><a href="https://when.data.coop/">Rallly</a> – find et tidspunkt (alternativ til Doodle)</h3>

<section class="service-details">
    <p>
        Lav afstemninger og find i fællesskab ud af hvilken dag og evt.
        tidspunkt du skal mødes med familie og venner. Ligesom Doodle var, før
        Doodle blev ødelagt.
    </p>
    <h4>Badges</h4>
    <ul class="badges">
        <li>
            <span class="badge badge-negative badge-stable">Negativ:</span>
            <a href="/tjenester/badges/#stabilitet">
                Tjenesten er ustabil
            </a>[1]
        </li>
        <li>
            <span class="badge badge-positive badge-secure-connection">Positiv:</span>
            <a href="/tjenester/badges/#sikker-forbindelse">
                Sikker forbindelse
            </a>
        </li>
        <li>
            <span class="badge badge-negative badge-encrypted-data-storage">Negativ:</span>
            <a href="/tjenester/badges/#krypteret-opbevaring-af-data">
                Data opbevares <em>ikke</em> krypteret
            </a>
        </li>
        <li>
            <span class="badge badge-negative badge-zero-knowledge">Negativ:</span>
            <a href="/tjenester/badges/#zero-knowledge">
                <em>Ikke</em> omfattet af zero knowledge
            </a>
        </li>
        <li>
            <span class="badge badge-positive badge-backup">Positiv:</span>
            <a href="/tjenester/badges/#backup">
                Der er backup af data
            </a>
        </li>
        <li>
            <span class="badge badge-negative badge-logging">Negativ:</span>
            <a href="/tjenester/badges/#logning">
                Der foretages logning
            </a>
        </li>
        <li>
            <span class="badge badge-partial badge-anonymous-access">Delvis:</span>
            <a href="/tjenester/badges/#anonym-adgang">
                Delvis anonym adgang
            </a>[2]
        </li>
    </ul>

    <p class="badge-notes">
        [1] Da tjenesten er lanceret for nyligt, kan vi endnu ikke sige noget
        om dens stabilitet.
    </p>
    <p class="badge-notes">
        [2] Personen der opretter en afstemning skal oplyse sin e-mailadresse.
        Når man deltager i en afstemning, skal man oplyse et navn. Dette kan
        selvfølgelig være fiktivt.
    </p>
</section>

<h3 class="service passit"><a href="https://passit.data.coop/">Passit</a> – password manager</h3>

<section class="service-details">
    <p>Hjælper dig med at huske stærke, unikke kodeord til alle de sites og apps du benytter dig af.</p>
    <h4>Badges</h4>
    <ul class="badges">
        <li>
            <span class="badge badge-positive badge-stable">Positiv:</span>
            <a href="/tjenester/badges/#stabilitet">
                Tjenesten er stabil
            </a>
        </li>
        <li>
            <span class="badge badge-positive badge-secure-connection">Positiv:</span>
            <a href="/tjenester/badges/#sikker-forbindelse">
                Sikker forbindelse
            </a>
        </li>
        <li>
            <span class="badge badge-positive badge-encrypted-data-storage">Positiv:</span>
            <a href="/tjenester/badges/#krypteret-opbevaring-af-data">
                Data opbevares krypteret
            </a>
        </li>
        <li>
            <span class="badge badge-partial badge-zero-knowledge">Delvis:</span>
            <a href="/tjenester/badges/#zero-knowledge">
                Omfattet af zero knowledge
            </a>[1]
        </li>
        <li>
            <span class="badge badge-positive badge-backup">Positiv:</span>
            <a href="/tjenester/badges/#backup">
                Der er backup af data
            </a>
        </li>
        <li>
            <span class="badge badge-negative badge-logging">Negativ:</span>
            <a href="/tjenester/badges/#logning">
                Der foretages logning
            </a>
        </li>
        <li>
            <span class="badge badge-negative badge-anonymous-access">Negativ:</span>
            <a href="/tjenester/badges/#anonym-adgang">
                Der er <em>ikke</em> anonym adgang
            </a>
        </li>
    </ul>

    <p class="badge-notes">
        [1] Al brugerdata (navne, websites, kodeord, noter osv.) er krypteret så det kun er brugerne selv der kan tilgå deres data. Der foretages dog stadig logning af brugen af tjenesten, så helt 100% ZN er det ikke.
    </p>
</section>

<h3 class="service gitea"><a href="https://git.data.coop/">Gitea</a> – hosting af <a href="https://da.wikipedia.org/wiki/Git">git-versionsstyringssystem</a></h3>

<section class="service-details">
    <p>Skriver du kode eller anden tekst som du gerne vil holde under versionsstyring, kan du gemme det her.</p>
    <h4>Badges</h4>
    <ul class="badges">
        <li>
            <span class="badge badge-positive badge-stable">Positiv:</span>
            <a href="/tjenester/badges/#stabilitet">
                Tjenesten er stabil
            </a>
        </li>
        <li>
            <span class="badge badge-positive badge-secure-connection">Positiv:</span>
            <a href="/tjenester/badges/#sikker-forbindelse">
                Sikker forbindelse
            </a>
        </li>
        <li>
            <span class="badge badge-negative badge-encrypted-data-storage">Negativ:</span>
            <a href="/tjenester/badges/#krypteret-opbevaring-af-data">
                Data opbevares <em>ikke</em> krypteret
            </a>
        </li>
        <li>
            <span class="badge badge-negative badge-zero-knowledge">Negativ:</span>
            <a href="/tjenester/badges/#zero-knowledge">
                <em>Ikke</em> omfattet af zero knowledge
            </a>
        </li>
        <li>
            <span class="badge badge-positive badge-backup">Positiv:</span>
            <a href="/tjenester/badges/#backup">
                Der er backup af data
            </a>
        </li>
        <li>
            <span class="badge badge-negative badge-logging">Negativ:</span>
            <a href="/tjenester/badges/#logning">
                Der foretages logning
            </a>
        </li>
        <li>
            <span class="badge badge-partial badge-anonymous-access">Delvis:</span>
            <a href="/tjenester/badges/#anonym-adgang">
                Delvis anonym adgang
            </a>[1]
        </li>
    </ul>

    <p class="badge-notes">
        [1] Offentligt tilgængelige repositories kan tilgås af alle, uden at være logget ind.
    </p>
</section>
