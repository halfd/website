# The data.coop website

[![Build Status](https://drone.data.coop/api/badges/data.coop/website/status.svg)](https://drone.data.coop/data.coop/website)

## Building the site

The site is built using [Jekyll](https://jekyllrb.com)
The simplest way to work with the site and code is to use Docker.

```
git clone https://git.data.coop/data.coop/website.git data.coop  
cd data.coop  
docker-compose up
```

This will fire up the website inside a Docker container and start listening on port 4000.
Simply connect your browser to http://localhost:4000 and you're good to go. In development
mode, the site has the [jekyll:livereload](https://github.com/RobertDeRose/jekyll-livereload) extension enabled, allowing you to see your changes immediately
upon saving files in the source directory.

If you change anything in `Gemfile` you need to run `docker-compose run jekyll bundle update` to update all your dependencies. 

## Deploying the site

Simply pushing to `master` in our main repo at https://git.data.coop/data.coop/website/ will trigger a build-and-deploy of the website.
